﻿using UnityEngine;
using System.Collections;

public class GamePadController1 : MonoBehaviour {

    private Transform m_transform;
    public float speed = 10.0f;
    public float rotSpeed = 160.0f;

    void Start()
    {

        m_transform = transform;

    }


    void Update()
    {

        //m_transform.position += m_transform.forward * Input.GetAxis("Vertical") * speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.I))
        {
            m_transform.position += m_transform.forward * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.K))
        {
            m_transform.position -= m_transform.forward * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.J))
        {
            transform.Rotate(Vector3.up * rotSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.L))
        {
            transform.Rotate(Vector3.down * rotSpeed * Time.deltaTime);
        }

    }
}
