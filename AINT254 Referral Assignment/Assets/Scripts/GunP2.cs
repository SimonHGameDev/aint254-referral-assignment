﻿using UnityEngine;
using System.Collections;

public class GunP2 : MonoBehaviour {

    public GameObject bulletPrefab;
    private Transform m_transform;
    public Transform shootFrom;

    void Start () {

        m_transform = transform;

    }
	

	void Update () {

        if (Input.GetKeyDown(KeyCode.O))
        {
            Instantiate(bulletPrefab, shootFrom.position, shootFrom.rotation);
        }

        if (Input.GetKeyDown(KeyCode.Joystick1Button0))
        {
            Instantiate(bulletPrefab, shootFrom.position, shootFrom.rotation);
        }

    }
}
