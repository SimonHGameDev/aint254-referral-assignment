﻿using UnityEngine;
using System.Collections;

public class KeyboardController1 : MonoBehaviour {

    private Transform m_transform;
    public float speed = 10.0f;
    public float rotSpeed = 160.0f;

    void Start()
    {

        m_transform = transform;

    }


    void Update()
    {

        //m_transform.position += m_transform.forward * Input.GetAxis("Vertical") * speed * Time.deltaTime;
        if (Input.GetKey(KeyCode.W))
        {
            m_transform.position += m_transform.forward * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.S))
        {
            m_transform.position -= m_transform.forward * speed * Time.deltaTime;
        }

        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.up * rotSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.down * rotSpeed * Time.deltaTime);
        }


        if (Input.GetAxis("VerticalP2") >= 0.3)
        {
            m_transform.position -= m_transform.forward * speed * Time.deltaTime;
        }

        if (Input.GetAxis("VerticalP2") <= -0.3)
        {
           m_transform.position += m_transform.forward * speed * Time.deltaTime;
        }

        if (Input.GetAxis("HorizontalP2") >= 0.3)
        {
            transform.Rotate(Vector3.up * rotSpeed * Time.deltaTime);
        }

        if (Input.GetAxis("HorizontalP2") <= -0.3)
        {
            transform.Rotate(Vector3.down * rotSpeed * Time.deltaTime);
        }


    }
}
