﻿using UnityEngine;
using System.Collections;

public class EnemyT1Bullet : MonoBehaviour {

    public float force = 1f;

    private Rigidbody m_rigidbody;
    private Transform m_transform;

    void Start () {

        m_transform = transform;
        m_rigidbody = GetComponent<Rigidbody>();

        m_rigidbody.AddForce(m_transform.forward * force / 3, ForceMode.Impulse);

        Destroy(gameObject, 7.0f);

    }
	
	
	void Update () {
	
	}
}
