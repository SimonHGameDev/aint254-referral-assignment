﻿using UnityEngine;
using System.Collections;

public class BulletP2 : MonoBehaviour {

    public float force = 60;

    private Rigidbody m_rigidbody;
    private Transform m_transform;

    void Start () {

        m_transform = transform;
        m_rigidbody = GetComponent<Rigidbody>();

        m_rigidbody.AddForce(m_transform.forward * force, ForceMode.Impulse);

        Destroy(gameObject, 1.4f);

    }
	

	void Update () {
	
	}
}
