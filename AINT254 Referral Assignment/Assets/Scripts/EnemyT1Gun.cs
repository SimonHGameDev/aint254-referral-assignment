﻿using UnityEngine;
using System.Collections;

public class EnemyT1Gun : MonoBehaviour {

    public GameObject bulletPrefab;
    private Transform m_transform;
    public Transform shootFrom;

    public Transform lookAtObj;
    private bool canFire = true;

    public float fireRate = 0.35f;
    private float fireShot = 0.0f;

    void Start () {

        m_transform = transform;

        

    }

	void Update () {


        //shootFrom.LookAt(lookAtObj.position);
        //Instantiate(bulletPrefab, shootFrom.position, shootFrom.rotation);
        if (canFire == true && Time.time > fireShot)
        {

            shootFrom.LookAt(lookAtObj.position);
            Instantiate(bulletPrefab, shootFrom.position, shootFrom.rotation);

            StartCoroutine(EnemyFire());

            fireShot = Time.time + fireRate;
        }
        


    }

    IEnumerator EnemyFire()
    {
        if(canFire == false)
        {
            canFire = false;

            yield return new WaitForSeconds(3f);

            canFire = true;
        }


    }
}
